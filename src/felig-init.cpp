/*******************************************************************/
/*                                                                 */
/* C++ source code for felig-inti tool                             */
/*                                                                 */
/* Author: Ablet, CERN , email: yiming.abulaiti@cern.ch            */
/*                                                                 */
/** Jan 2019. Get some code from flx-config tool  ******************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <iostream>
#include <iomanip>
#include "version.h"
#include "DFDebug/DFDebug.h"
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include <getopt.h>

using namespace std;

#define APPLICATION_NAME    "felig-init"

//usage
static void print_help(){
    
    printf ("-d, %-10s %s\n", "N","Use device number N. [Default: 0]");
    printf ("-v, %-10s %s\n", "--verbose","Print additional debug information.");
    printf ("-h, %-10s %s\n", "--help","Print help and exit.");
    printf ("-V, %-10s %s\n", " ","Print version and exit.");
    printf("--chunk_length\n \t%s\n \t%s\n \t%s\n",
           "Chunk Length in hex [bytes].",
           "Must pad with 4 '0's",
           "Example: --chunk_length=0x3C"
           );
    printf("--endian_mode\n \t%s\n \t%s\n \t%s\n",
           "Elink endian mode. One bit per egroup.",
           "0x00 =  little-endian",
           "0x1F =  big-endian"
           );
    printf("--data_format\n \t%s\n \t%s\n \t%s\n",
           "Elink data format. One bit per egroup.",
           "0x1F =  8b10b mode",
           "0x00 =  direct mode"
           );
    printf("--elink_input_width\n \t%s\n \t%s\n \t%s\n",
           "Elink input data width. One bit per egroup.",
           "0x00 =  8-bit (direct mode)",
           "0x1F =  10-bit (8b10b mode)"
           );
    printf("--elink_output_width\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n",
           "Elink output data width. One bit per egroup.",
           "For Each E-Group",
           "0x0 = all e-links set to 2 bit",
           "0x55 = all e-links enabled, for 4-bit width",
           "0x11 = all e-links enabled, for 8-bit width",
           "0x01 = all e-links enabled, for 16-bit width"
           );
    printf("--elink_enable\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n",
           "For Each E-Group",
           "0x00 = all e-links disabled",
           "0xFF = all e-links enabled, for 2-bit width",
           "0x55 = all e-links enabled, for 4-bit width",
           "0x11 = all e-links enabled, for 8-bit width",
           "0x01 = all e-links enabled, for 16-bit width"
           );
    printf("--b_channel_select\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n",
           "TTC Config. Select b-channel data bit from GBT word.",
           "0x01 = Elink0 - 2-bit TTC",
           "0x03 = Elink0 - 4-bit TTC",
           "0x07 = Elink0 - 8-bit TTC",
           "0xFF = unused");
    exit(0);
}

//Globals
FlxCard flxCard;

//Debug
bool debug=false;

//Total number of channels
int channels=24;

// Chunk Length in hex [bytes].
// Must pad with 4 '0's
//chunk_length="0x20"
const char *chunk_length="0x3C";

// Elink endian mode
// One bit per egroup
// 0x00 =  little-endian
// 0x1F =  big-endian
const char *endian_mode="0x00";

// Elink data format
// One bit per egroup
// 0x1F =  8b10b mode
// 0x00 =  direct mode
const char *data_format="0x1F";

// Elink input data width
//One bit per egroup
// 0x00 =  8-bit (direct mode)
// 0x1F =  10-bit (8b10b mode)
const char *elink_input_width="0x1F";

//// Elink output data width
// Two bits per egroup
//
// For Each E-Group
// 0x0 = all e-links set to 2 bit
// 0x1 = all e-links set to 4 bit
// 0x2 = all e-links set to 8 bit
// 0x3 = all e-links set to 16 bit
const char *elink_output_width="0x250";

//// Elink Enable
// Following example values assume all elinks are enabled
// within an egroup for a giv elink width.
//
// For Each E-Group
// 0x00 = all e-links disabled
// 0xFF = all e-links enabled, for 2-bit width
// 0x55 = all e-links enabled, for 4-bit width
// 0x11 = all e-links enabled, for 8-bit width
// 0x01 = all e-links enabled, for 16-bit width
//elink_enable="0x00FF555533"
const char *elink_enable="0x115555FF00";

// As configured:
/* E-Group 1 = All e-links disabled
# E-Group 2 = all (2-bit) e-links enabled
# E-Group 3 = all (4-bit) e-links enabled
# E-Group 4 = all (4-bit) e-links enabled
# E-Group 5 = all (8-bit) e-links enabled

### TTC Config
# Select b-channel data bit from GBT word.
#
# examples:
# "0x01" = Elink0 - 2-bit TTC
# "0x03" = Elink0 - 4-bit TTC
# "0x07" = Elink0 - 8-bit TTC
# "0xFF" = unused
#b_channel_select="0x01"
 */
const char *b_channel_select="0x21";


/*Configuring reference clock 1...*/
//flxCard.i2c_devices_write(const char *device, u_char reg_addr, u_char data);
void config_reference_clock (const char *dev, const char *reg_addr, const char *value){
    
    u_int converted_value;
    u_char i2caddr, i2cdata;
    
    sscanf(value, "%x", &converted_value);
    i2cdata = converted_value;
    
    sscanf(reg_addr, "%x", &converted_value);
    i2caddr = converted_value;

    
    if(debug){
        printf("Now writing 0x%02x to register 0x%02x of device %s\n", i2cdata, i2caddr, dev);
    }
    try{
        flxCard.i2c_devices_write(dev, i2caddr, i2cdata);
    }
    catch (FlxException excep){
        cout << "ERROR. Exception thrown: " << excep.what() << endl;
        exit(-1);
    }
    
}

//flxCard.i2c_devices_read(const char *device, u_char reg_addr, u_char *value);
void config_reference_clock_read (const char *dev, const char *reg_addr){
    u_char result = 0;
    u_int converted_value;
    u_char i2caddr;
    
    sscanf(reg_addr, "%x", &converted_value);
    i2caddr = converted_value;
    
    if(debug){
        printf("Now reading from register 0x%02x of device %s\n", i2caddr, dev);
    }
    
    try{
        flxCard.i2c_devices_read(dev, i2caddr, &result);
    } catch(FlxException excep){
        std::cout << "ERROR. Exception thrown: " << excep.what() << std:: endl;
        exit(-1);

    }
}

void config_reference_clock_all ( ){
    
    config_reference_clock("2:0:0x6E", "0x0000", "0xEA");
    config_reference_clock("2:0:0x6E", "0x0001", "0xEA"); 
    config_reference_clock("2:0:0x6E", "0x0002", "0xEA"); 
    config_reference_clock("2:0:0x6E", "0x0003", "0xEA"); 
    config_reference_clock("2:0:0x6E", "0x0004", "0x15"); 
    config_reference_clock("2:0:0x6E", "0x0005", "0x15"); 
    config_reference_clock("2:0:0x6E", "0x0006", "0x15"); 
    config_reference_clock("2:0:0x6E", "0x0007", "0x15"); 
    config_reference_clock("2:0:0x6E", "0x0008", "0x00"); 
    config_reference_clock("2:0:0x6E", "0x0009", "0x00"); 
    config_reference_clock("2:0:0x6E", "0x000A", "0x00"); 
    config_reference_clock("2:0:0x6E", "0x000B", "0x00"); 
    config_reference_clock("2:0:0x6E", "0x000C", "0x8A"); 
    config_reference_clock("2:0:0x6E", "0x000D", "0x8A"); 
    config_reference_clock("2:0:0x6E", "0x000E", "0x8A"); 
    config_reference_clock("2:0:0x6E", "0x000F", "0x8A"); 
    config_reference_clock("2:0:0x6E", "0x0012", "0x00"); 
    config_reference_clock("2:0:0x6E", "0x0014", "0x0F"); 
    config_reference_clock("2:0:0x6E", "0x0015", "0x0F"); 
    config_reference_clock("2:0:0x6E", "0x0016", "0x0F"); 
    config_reference_clock("2:0:0x6E", "0x0017", "0x0F"); 
    
    //reinit the PLL
    config_reference_clock("2:0:0x6E", "0x0012", " 0xA0");
    config_reference_clock("2:0:0x6E", "0x0012", " 0xB8"); 
    config_reference_clock("2:0:0x6E", "0x0012", " 0xA0");
    
    //echo " Next Line should return 0xA0..."
    //$flxi2c r 2:0:0x6E 0x0012 | grep Register
    config_reference_clock_read("2:0:0x6E", "0x0012");
    
    //" Configuring reference clock 2..."
    
    config_reference_clock("3:0:0x6E", "0x0000", "0x2A"); 
    config_reference_clock("3:0:0x6E", "0x0001", "0x2A"); 
    config_reference_clock("3:0:0x6E", "0x0002", "0x2A"); 
    config_reference_clock("3:0:0x6E", "0x0003", "0x2A"); 
    config_reference_clock("3:0:0x6E", "0x0004", "0x15"); 
    config_reference_clock("3:0:0x6E", "0x0005", "0x15"); 
    config_reference_clock("3:0:0x6E", "0x0006", "0x15"); 
    config_reference_clock("3:0:0x6E", "0x0007", "0x15"); 
    config_reference_clock("3:0:0x6E", "0x0008", "0x00"); 
    config_reference_clock("3:0:0x6E", "0x0009", "0x00"); 
    config_reference_clock("3:0:0x6E", "0x000A", "0x00"); 
    config_reference_clock("3:0:0x6E", "0x000B", "0x00"); 
    config_reference_clock("3:0:0x6E", "0x000C", "0x8A"); 
    config_reference_clock("3:0:0x6E", "0x000D", "0x8A"); 
    config_reference_clock("3:0:0x6E", "0x000E", "0x8A"); 
    config_reference_clock("3:0:0x6E", "0x000F", "0x8A"); 
    config_reference_clock("3:0:0x6E", "0x0014", "0x0F"); 
    config_reference_clock("3:0:0x6E", "0x0015", "0x0F"); 
    config_reference_clock("3:0:0x6E", "0x0016", "0x0F"); 
    config_reference_clock("3:0:0x6E", "0x0017", "0x0F"); 
    
    //reinit the PLL
    config_reference_clock("3:0:0x6E", "0x0012", "0xA0");
    config_reference_clock("3:0:0x6E", "0x0012", "0xB8");
    config_reference_clock("3:0:0x6E", "0x0012", "0xA0");
    
   // Next Line should return 0xA0...
    config_reference_clock_read("3:0:0x6E", "0x0012");
}

//Setting global controls
void set_global_control(const char *key_char, const char *value_c){
    u_long converted_value =0;
    sscanf(value_c, "%x", &converted_value);
    
    if(debug){
        cout<<key_char<<" = "<<std::hex<<converted_value<<"(base 16) | "<<std::dec<<converted_value<<" (base 10)"<<endl;
    }
    flxCard.cfg_set_option(key_char, converted_value);
    
}

void set_global_control_all(){
    
    if(debug){
        cout<<"Setting global controls..."<<endl;
    }
    
    //set local trigger period. [25ns / LSB]
    set_global_control("FELIG_GLOBAL_CONTROL_FAKE_L1A_RATE", "0x00000000" );
    set_global_control("FELIG_GLOBAL_CONTROL_PICXO_OFFSET_PPM", "0x00000" );
    set_global_control("FELIG_GLOBAL_CONTROL_TRACK_DATA", "1" );
    set_global_control("FELIG_GLOBAL_CONTROL_TRACK_DATA", "1" );
    set_global_control("FELIG_GLOBAL_CONTROL_RXUSERRDY", "1" );
    set_global_control("FELIG_GLOBAL_CONTROL_TXUSERRDY", "1" );
    set_global_control("FELIG_GLOBAL_CONTROL_AUTO_RESET", "1" );
    set_global_control("FELIG_GLOBAL_CONTROL_PICXO_RESET", "1" );
    set_global_control("FELIG_GLOBAL_CONTROL_GTTX_RESET", "1" );
    set_global_control("FELIG_GLOBAL_CONTROL_CPLL_RESET", "1" );
    set_global_control("FELIG_GLOBAL_CONTROL_X3_X4_OUTPUT_SELECT", "0x00" );
    
    //Release Global Resets
    set_global_control("FELIG_GLOBAL_CONTROL_PICXO_RESET", "0" );
    set_global_control("FELIG_GLOBAL_CONTROL_GTTX_RESET", "0" );
    set_global_control("FELIG_GLOBAL_CONTROL_CPLL_RESET", "0" );
    
    //Toggle Lane Resets
    set_global_control("FELIG_RESET_LANE", "0xFFFFFF" );
    set_global_control("FELIG_RESET_LANE", "0x000000" );
    
    //Toggle framegen resets.  (shouldn't be required.)
    set_global_control("FELIG_RESET_FRAMEGEN", "0xFFFFFF" );
    set_global_control("FELIG_RESET_FRAMEGEN", "0x000000" );
    
    //Toggle Loopback FIFO Resets
    set_global_control("FELIG_RESET_LB_FIFO", "0xFFFFFF" );
    set_global_control("FELIG_RESET_LB_FIFO", "0x000000" );
    
    //Toggle GBT RX Resets
    set_global_control("GBT_GTRX_RESET", "0xFFFFFFFFFF" );
    set_global_control("GBT_GTRX_RESET", "0x0000000000" );
}

//Get this from flx-confit.cpp
void set_option_ch(const char *key_char, const char *c_value, u_short ch = 0){
    
    char key[sizeof(key_char)/sizeof(key_char[0])];
    sprintf(key, key_char, ch);
    
    //string ll;
    u_long value = strtoll(c_value, NULL, 0);
    
    if(debug){
        cout<<key<<" = "<<std::hex<<value<<"(base 16) | "<<std::dec<<value<<"(base 10)"<<endl;
    }
    
    try{
        flxCard.cfg_set_option(key, value);
        //cout<<key<<" = "<<value<<endl;
    }
    catch (FlxException excep){
        cout << "ERROR. Exception thrown: " << excep.what() << endl;
        exit(-1);
    }
    
    //delete[] key;
}

//appy setup for 24 ch
void set_option_all (){
    for (u_short i=0; i<channels; i++) {
        set_option_ch("FELIG_DATA_GEN_CONFIG_%02u_USERDATA","0x0000", i);
        set_option_ch("FELIG_DATA_GEN_CONFIG_%02u_CHUNK_LENGTH",chunk_length, i);
        set_option_ch("FELIG_DATA_GEN_CONFIG_%02u_RESET","0x00", i);
        set_option_ch("FELIG_DATA_GEN_CONFIG_%02u_SW_BUSY","0x00", i);
        set_option_ch("FELIG_DATA_GEN_CONFIG_%02u_DATA_FORMAT", data_format, i);
        set_option_ch("FELIG_DATA_GEN_CONFIG_%02u_PATTERN_SEL","0x00", i);
        set_option_ch("FELIG_ELINK_CONFIG_%02u_ENDIAN_MOD", endian_mode, i);
        set_option_ch("FELIG_ELINK_CONFIG_%02u_INPUT_WIDTH", elink_input_width, i);
        set_option_ch("FELIG_ELINK_CONFIG_%02u_OUTPUT_WIDTH", elink_output_width, i);
        set_option_ch("FELIG_ELINK_ENABLE_%02u", elink_enable, i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_LB_FIFO_DELAY", "0x02", i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_ELINK_SYNC", "0", i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_PICXO_OFFEST_EN", "0", i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_PI_HOLD", "0", i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_GBT_LB_ENABLE", "0", i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_GBH_LB_ENABLE", "0", i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_FG_SOURCE", "0", i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_B_CH_BIT_SEL", b_channel_select, i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_A_CH_BIT_SEL", "0xFF", i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_L1A_SOURCE", "1", i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_GBT_EMU_SOURCE", "0", i);
        
        
    }
    
}

//Toggle Lane Resets
void resets(){
    set_option_ch("FELIG_RESET_LANE", "0xFFFFFF");
    set_option_ch("FELIG_RESET_LANE", "0x000000");

    //Toggle framegen resets.  (shouldn't be required.)
    set_option_ch("FELIG_RESET_FRAMEGEN", "0xFFFFFF");
    set_option_ch("FELIG_RESET_FRAMEGEN", "0x000000");
    
    //Toggle Loopback FIFO Resets
    set_option_ch("FELIG_RESET_LB_FIFO", "0xFFFFFF");
    set_option_ch("FELIG_RESET_LB_FIFO", "0x000000");
}

void elink_sync(){
    for (int i=0; i<channels; i++) {
        set_option_ch("FELIG_LANE_CONFIG_%02u_ELINK_SYNC", "0xFFFFFF", i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_ELINK_SYNC", "0x000000", i);
    }
}

//main function
int main(int argc /*Number of arguments*/, char **argv/*Pointer array to arguments*/){
    
    //parse options
    int opt=0;
    int device_number = 0;
    
    //loop over argv[] and read option strings
    while ( 1 ){
        int option_index = 0;
        static struct option long_options[] = {
            {"chunk_length",     required_argument, 0,  'c' },
            {"endian_mode",     required_argument, 0,  'm' },
            {"data_format",     required_argument, 0,  'f' },
            {"elink_input_width",     required_argument, 0,  'i' },
            {"elink_output_width",     required_argument, 0,  'o' },
            {"elink_enable",     required_argument, 0,  'e' },
            {"b_channel_select",     required_argument, 0,  'b' },
            {0,         0,                 0,  0 }
        };
        
        opt = getopt_long(argc, argv, "hd:Vv", long_options, &option_index);
        if(opt==-1) break;
        
        switch (opt){
            case 'h':
                print_help();
                break;
            
            case 'd':
                device_number = atoi(optarg);
                break;
                
            case 'V':
                break;
                
            case 'v':
                debug=true;
                break;
            case 'c':
                chunk_length = optarg;
                break;
            case 'm':
                endian_mode = optarg;
                break;
            case 'f':
                data_format = optarg;
                break;
            case 'i':
                elink_input_width = optarg;
                break;
            case 'o':
                elink_output_width = optarg;
                break;
            case 'e':
                elink_enable = optarg;
                break;
            case 'b':
                b_channel_select = optarg;
                break;
            default:{
                    //should print something.
                    print_help();
                }
                break;
        }//end of switch
        
    }//end of while loop
    
    try
    {
        flxCard.card_open(device_number, LOCK_I2C);
    }
    catch(FlxException &ex)
    {
        std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
        exit(-1);
    }

    //configure reference clock
    config_reference_clock_all();
    
    try
    {
        flxCard.card_close();
    }
    catch(FlxException &ex)
    {
        std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
        exit(-1);
    }

    
    
    try
    {
        flxCard.card_open(device_number, LOCK_ALL);
    }
    catch(FlxException ex)
    {
        std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
        exit(-1);
    }
    
    //get channels.
    channels = flxCard.cfg_get_option(BF_NUM_OF_CHANNELS);
    
    //FELIG global configuration
    set_global_control_all();
    
    //FELIG lane configuration
    set_option_all();
    
    // Toggle Lane Resets
    resets();
    
    //FELIG LANE CONFIG -- ELINK_SYNC
    elink_sync();
    
    try
    {
        flxCard.card_close();
    }
    catch(FlxException ex)
    {
        std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
        exit(-1);
    }

}// end of main()
