# felig-tools

Jira ticket:
https://its.cern.ch/jira/browse/FLX-802

# install  

Checkout from this repository. Put under software/ directory.  

run cmake_config x86***   

cd x86**/ and build with make.  

 

I have been using this for FELIG <--> test.  

example commands:  
  
../software/x86_64-centos7-gcc62-opt/felig-tools/felig-init -d 0  
../software/x86_64-centos7-gcc62-opt/felig-tools/felig-init -d 1  
../software/x86_64-centos7-gcc62-opt/felig-tools/felig-start --emu=sm --local-trigger -d 0  
../software/x86_64-centos7-gcc62-opt/felig-tools/felig-start --emu=sm --local-trigger -d 1  
  
../software/x86_64-centos7-gcc62-opt/felig-tools/felig-stop -d 0  
../software/x86_64-centos7-gcc62-opt/felig-tools/felig-stop -d 1  